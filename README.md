This project was created to pull the html data from http://legacy.aonprd.com/.  Paizo decided to stop supporting this site without warning.  This site is going to be restructured and rolled into another site.  They broke the built in search feature and all my old links.  I decided to pull all their content so I would have a local copy.

I followed this tutorial for the the basis of this project.
http://www.netinstructions.com/how-to-make-a-simple-web-crawler-in-javascript-and-node-js/